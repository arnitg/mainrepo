<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin Area • Crocodile&trade;</title>
    <style>
        <?php include 'CSS/adminArea.css'; ?>
    </style>
    <link rel="icon" href="images/favicon.png"/>
    <script src="JS/toTopButton.js"></script>
    <link rel="stylesheet" href="CSS/toTopButton.css">
</head>
<body>
<div class="sidenav">
    <a href="#mainSliderDiv">Main</a>
    <a href="#ourstoryy">Our Story</a>
    <a href="#product">Product</a>
    <a href="#photos">Photos</a>
    <a href="#brands">Brands</a>
</div>

<div class="content">

    <div id="mainSliderDiv">
        <h1>Main</h1>

        <div id="mainSliderDiv">
            <form class="a" action="" method="post" enctype="multipart/form-data">
                <label for="title">Main Slider</label><br><br>
                <label for="countdown">Change picture 1</label>
                <input type="file" class="file" id="fileUpload"name=" fileToUpload"><br><br>
                <label for="countdown">Change picture 2</label>
                <input type="file"  class="file" name="fileToUpload1"><br><br>
                <label for="countdown">Change picture 3</label>
                <input type="file"  class="file" name="fileToUpload2"><br><br>
                <input type="submit" name="saveSlider" value="Submit">
            </form>
        </div>

        <div id="videos">
            <form action="" class="a" method="post" enctype="multipart/form-data">
                <label for="title">Videos</label><br><br>
                <label for="countdown">Change video 1</label>
                <input type="file"  class="file" name="video1"><br><br>
                <label for="countdown">Change video 2</label>
                <input type="file"  class="file" name="video2"><br><br>
                <input type="submit" name="saveVideo" value="Submit">
            </form>
        </div>

        <div id="bottomSlider">
            <form action="" class="a" method="post" enctype="multipart/form-data">
                <label for="title">Bottom Slider</label><br><br>
                <label >Change picture 1</label>
                <input type="file" class="file" name="slider1"><br><br>
                <label >Change picture 2</label>
                <input type="file" class="file" name="slider2"><br><br>
                <label >Change picture 3</label>
                <input type="file" class="file" name="slider3"><br><br>
                <label >Change picture 4</label>
                <input type="file" class="file" name="slider4"><br><br>
                <label >Change picture 5</label>
                <input type="file" class="file" name="slider5"><br><br>
                <label >Change picture 6</label>
                <input type="file" class="file" name="slider6"><br><br>
                <label>Change picture 7</label>
                <input type="file" class="file" name="slider7"><br><br>
                <label >Change picture 8</label>
                <input type="file" class="file" name="slider8"><br><br>
                <label >Change picture 9</label>
                <input type="file" class="file" name="slider9"><br><br>
                <label >Change picture 10</label>
                <input type="file" class="file" name="slider10"><br><br>
                <input type="submit" name="saveSlider2" value="Submit">
            </form>
        </div>
    </div>

    <hr>

    <div id="ourstoryy">
        <h1>Our Story</h1>

        <div class="ourStory" id="ourstoryy">
            <form action="" class="b" method="post" enctype="multipart/form-data">
                <label for="title">Change Connected.</label><br><br>
                <input type="text"  name="title1" placeholder="title"><br>
                <input type="file" class="file" name="picture1" placeholder="picture"><br><br>
                <input type="text"  name="text1" placeholder="text"><br><br>
                <input type="submit" name="connected" value="Submit"><br>
            </form>
        </div>

        <div class="ourStory" >
            <form action="" class="b" method="post" enctype="multipart/form-data">
                <label for="title">Change Shift.</label><br><br>
                <input type="text"  name="title2" placeholder="title"><br>
                <input type="file" class="file" name="picture2" placeholder="picture"><br><br>
                <input type="text"  name="text2" placeholder="text"><br><br>
                <input type="submit" name="shift" value="Submit"><br>
            </form>
        </div>


        <div class="ourStory">
            <form action="" class="b" method="post" enctype="multipart/form-data">
                <label for="title">Change Accelerated.</label><br><br>
                <input type="text"  name="title3" placeholder="title"><br>
                <input type="file" class="file" name="picture3" placeholder="picture"><br><br>
                <input type="text"  name="text3" placeholder="text"><br><br>
                <input type="submit" name="accelerated" value="Submit"><br>
            </form>
        </div>

        <div class="ourStory">
            <form action="" class="b" method="post" enctype="multipart/form-data">
                <label for="title">Change Garment.</label><br><br>
                <input type="text"  name="title4" placeholder="title"><br>
                <input type="file" class="file" name="picture4" placeholder="picture"><br><br>
                <input type="text"  name="text4" placeholder="text"><br><br>
                <input type="submit" name="garment" value="Submit"><br>
            </form>
        </div>

        <div class="ourStory">
            <form action="" class="b" method="post" enctype="multipart/form-data">
                <label for="title">Change Modern.</label><br><br>
                <input type="text"  name="title5" placeholder="title"><br>
                <input type="file" class="file" name="picture5" placeholder="picture"><br><br>
                <input type="text"  name="text5" placeholder="text"><br><br>
                <input type="submit" name="modern" value="Submit"><br>
            </form>
        </div>

        <div class="ourStory">
            <form action="" class="b" method="post" enctype="multipart/form-data">
                <label for="title">Change Reach.</label><br><br>
                <input type="text"  name="title6" placeholder="title"><br>
                <input type="file"  class="file" name="picture7" placeholder="picture"><br><br>
                <input type="text"  name="text6" placeholder="text"><br><br>
                <input type="submit" name="reach" value="Submit"><br>
            </form>
        </div>

        <div class="ourStory">
            <form action="" class="b" method="post" enctype="multipart/form-data">
                <label for="title">Change Contact.</label><br><br>
                <input type="text"  name="title7" placeholder="title"><br>
                <input type="file"  class="file" name="picture7" placeholder="picture"><br><br>
                <input type="text"  name="text7" placeholder="text"><br><br>
                <input type="submit" name="contact" value="Submit"><br><br>
            </form>
        </div>
    </div>

    <hr>

    <div id="product">
        <h1>Products</h1>
        <table>
            <tr>
                <th>Product ID</th>
                <th>Description</th>
                <th>Price</th>
                <th>Brands</th>
                <th>Category</th>
            </tr>
            <?php
            require_once 'classes/product.php';
            $produkti = new product(null,null,null,null,null);
            $check = 0;
            foreach ($produkti->getAll() as $row)
            {
                echo '<tr>';
                foreach ($row as $column)
                {
                    if($check%2==0) {
                        echo '<td>';
                        echo $column;
                        echo '</td>';

                    }
                    $check++;
                }
                echo '</tr>';
            }?>
        </table>
        <br>
        <br>
        <form action="" method="post" enctype="multipart/form-data">
            <input type="text"  name="id" placeholder="id"><br><br>
            <input type="text"  name="description" placeholder="description"><br><br>
            <input type="text"  name="price" placeholder="price"><br><br>
            <input type="text"  name="brands" placeholder="brands"><br><br>
            <input type="text"  name="category" placeholder="category"><br><br>
            <input type="submit" name="Save" value="Save">
            <input type="submit" name="Update" value="Update">
            <input type="submit" name="Delete" value="Delete"><br><br>
        </form>
    </div>

    <hr>

    <div id="photos">
        <h1>Photos</h1>

        <table>
            <tr>
                <th>Photo ID</th>
                <th>Path</th>
                <th>Product ID</th>
            </tr>
            <?php
            require_once 'classes/photos.php';
            $photos = new photos(null,null,null);
            $check = 0;
            foreach ($photos->getAll() as $row)
            {
                echo '<tr>';
                foreach ($row as $column)
                {
                    if($check%2==0) {
                        echo '<td>';
                        echo $column;
                        echo '</td>';

                    }
                    $check++;
                }
                echo '</tr>';
            }?>

        </table>
        <br>
        <br>
        <form action="" method="post" enctype="multipart/form-data">
            <input type="text"  name="id" placeholder="id"><br><br>
            <input type="text"  name="path" placeholder="picture"><br><br>
            <input type="text"  name="productID" placeholder="product ID"><br><br>
            <input type="submit" name="SaveP" value="Save">
            <input type="submit" name="UpdateP" value="Update">
            <input type="submit" name="DeleteP" value="Delete"><br><br>
        </form>
    </div>

    <hr>

    <div id="brands">
        <h1>Brands</h1>

        <form action="" method="post" enctype="multipart/form-data">
            <input type="text"  name="titulli1" placeholder="title"><br><br>
            <input type="file"  class="file" name="foto1" placeholder="picture"><br><br>
            <input type="text"  name="teksti1" placeholder="text"><br><br>
            <input type="submit" name="saveBrands" value="Submit"><br>
        </form>

        <div class="ourStory" >
            <form action="" class="b" method="post" enctype="multipart/form-data">
                <label for="title">SITE EXAMPLE</label><br><br>
                <input type="text"  name="titulli2" placeholder="title"><br>
                <input type="file"  name="foto2" placeholder="picture"><br><br>
                <input type="text"  name="teksti2" placeholder="text"><br><br>
                <input type="submit" name="saveSite" value="Submit">
            </form>
        </div>

        <div class="ourStory">
            <form action="" class="b" method="post" enctype="multipart/form-data">
                <label for="title">Protection Policies</label><br><br>
                <input type="text"  name="titulli3" placeholder="title"><br>
                <input type="file"  name="foto3" placeholder="picture"><br><br>
                <input type="text"  name="teksti3" placeholder="text"><br><br>
                <input type="submit" name="savePolicy" value="Submit">
            </form>
        </div>

        <div class="ourStory">
            <form action="" class="b" method="post" enctype="multipart/form-data">
                <label for="title">Quote</label><br><br>
                <input type="text"  name="titulli4" placeholder="title"><br>
                <input type="file"  name="foto4" placeholder="picture"><br><br>
                <input type="text"  name="teksti4" placeholder="text"><br><br>
                <input type="submit" name="saveQuote" value="Submit">
            </form>
        </div>
    </div>
    <hr>
</div>
</body>
</html>

<?php
require_once 'fileFinder.php';
require_once 'classes/photos.php';
require_once 'classes/product.php';

$test = new filePath();


if(isset($_POST['saveSlider']))
{
    $uploadfile = basename($_FILES['fileToUpload']['name']);
    $uploadfile1 = basename($_FILES['fileToUpload1']['name']);
    $uploadfile2 = basename($_FILES['fileToUpload2']['name']);
    $gang = $test->findFile(12,$uploadfile);
    $gang = $test->findFile(13,$uploadfile1);
    $gang = $test->findFile(14,$uploadfile2);
}
if (isset($_POST['saveVideo']))
{
    $uploadfile = basename($_FILES['video1']['name']);
    $uploadfile1 = basename($_FILES['video2']['name']);
    $gang = $test->findFile(27,$uploadfile);
    $gang = $test->findFile(28,$uploadfile1);
}
if (isset($_POST['saveSlider2']))
{
    $uploadfile = basename($_FILES['slider1']['name']);
    $uploadfile1 = basename($_FILES['slider2']['name']);
    $uploadfile2 = basename($_FILES['slider3']['name']);
    $uploadfile3 = basename($_FILES['slider4']['name']);
    $uploadfile4 = basename($_FILES['slider5']['name']);
    $uploadfile5 = basename($_FILES['slider6']['name']);
    $uploadfile6 = basename($_FILES['slider7']['name']);
    $uploadfile7 = basename($_FILES['slider8']['name']);
    $uploadfile8 = basename($_FILES['slider9']['name']);
    $uploadfile9 = basename($_FILES['slider10']['name']);

    $gang = $test->findFile(17,$uploadfile);
    $gang = $test->findFile(18,$uploadfile1);
    $gang = $test->findFile(19,$uploadfile2);
    $gang = $test->findFile(20,$uploadfile3);
    $gang = $test->findFile(21,$uploadfile4);
    $gang = $test->findFile(22,$uploadfile5);
    $gang = $test->findFile(23,$uploadfile6);
    $gang = $test->findFile(24,$uploadfile7);
    $gang = $test->findFile(25,$uploadfile8);
    $gang = $test->findFile(26,$uploadfile9);
}
if (isset($_POST['connected']))
{
    $title = $_POST['title1'];
    $uploadfile = basename($_FILES['picture1']['name']);
    $text = $_POST['text1'];
    $test->updateOurStory(1,$title,$uploadfile,$text);
}
if (isset($_POST['shift']))
{
    $title = $_POST['title2'];
    $uploadfile = basename($_FILES['picture2']['name']);
    $text = $_POST['text2'];
    $test->updateOurStory(2,$title,$uploadfile,$text);
}
if (isset($_POST['accelerated']))
{
    $title = $_POST['title3'];
    $uploadfile = basename($_FILES['picture3']['name']);
    $text = $_POST['text3'];
    $test->updateOurStory(3,$title,$uploadfile,$text);
}
if (isset($_POST['garment']))
{
    $title = $_POST['title4'];
    $uploadfile = basename($_FILES['picture4']['name']);
    $text = $_POST['text4'];
    $test->updateOurStory(4,$title,$uploadfile,$text);
}
if (isset($_POST['modern']))
{
    $title = $_POST['title5'];
    $uploadfile = basename($_FILES['picture5']['name']);
    $text = $_POST['text5'];
    $test->updateOurStory(5,$title,$uploadfile,$text);
}
if (isset($_POST['reach']))
{
    $title = $_POST['title6'];
    $uploadfile = basename($_FILES['picture6']['name']);
    $text = $_POST['text6'];
    $test->updateOurStory(6,$title,$uploadfile,$text);
}
if (isset($_POST['contact']))
{
    $title = $_POST['title7'];
    $uploadfile = basename($_FILES['picture7']['name']);
    $text = $_POST['text7'];
    $test->updateOurStory(7,$title,$uploadfile,$text);
}
/*----------------------------------------------product and pictures crud -------------------------------------------------*/
$product = new product(null,null,null,null,null);
if(isset($_POST['Save']))
{
    $description = $_POST['description'];
    $price = $_POST['price']+0;
    $brands = $_POST['brands'];
    $category = $_POST['category'];
    $produkti = new product(null,$description,$price,$brands,$category);
    $product->addProduct($produkti);
}
if(isset($_POST['Update']))
{
    $id = $_POST['id'];
    $description = $_POST['description'];
    $price = $_POST['price']+0;
    $brands = $_POST['brands'];
    $category = $_POST['category'];
    $produkti = new product($id,$description,$price,$brands,$category);
    $product->updateProduct($produkti);
}

if(isset($_POST['Delete']))
{
    $id = $_POST['id'];
    $product->deleteProduct($id);
}

$photos = new photos(null,null,null);

if(isset($_POST['SaveP']))
{
    $path = $_POST['path'];
    $prod_id = $_POST['productID'];
    $photo = new photos(null,$path,$prod_id);
    $photos->addPictures($photo);
}

if(isset($_POST['UpdateP']))
{
    $id = $_POST['id'];
    $path = $_POST['path'];
    $price = $_POST['productID'];
    $photo = new photos($id,$path,$price);
    $photos->updatePictures($photo);
}

if(isset($_POST['DeleteP']))
{
    $id = $_POST['id'];
    $photos->deletePictures($id);
}


if(isset($_POST['saveBrands']))
{
    $title = $_POST['titulli1'];
    $uploadfile = basename($_FILES['foto1']['name']);
    $text = $_POST['teksti1'];
    $test->updateOurStory(8,$title,$uploadfile,$text);
}

if(isset($_POST['saveSite']))
{
    $title = $_POST['titulli2'];
    $uploadfile = basename($_FILES['foto2']['name']);
    $text = $_POST['teksti2'];
    $test->updateOurStory(9,$title,$uploadfile,$text);
}

if(isset($_POST['savePolicy']))
{
    $title = $_POST['titulli3'];
    $uploadfile = basename($_FILES['foto3']['name']);
    $text = $_POST['teksti3'];
    $test->updateOurStory(10,$title,$uploadfile,$text);
}

if(isset($_POST['saveQuote']))
{
    $title = $_POST['titulli4'];
    $uploadfile = basename($_FILES['foto4']['name']);
    $text = $_POST['teksti4'];
    $test->updateOurStory(11,$title,$uploadfile,$text);
}
?>