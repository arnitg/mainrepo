<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Saved Iteams • Crocodile&trade;</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://kit.fontawesome.com/572ab64f26.js" crossorigin="anonymous"></script>
    <link rel="icon" href="images/favicon.png"/>
    <link rel="stylesheet" href="CSS/MainCss.css">
    <script src="JS/Throbber.js"></script>
    <style>
        <?php include 'CSS/Bag&Saved.css'?>
    </style>
</head>
<body>
<header>
    <script>
        <?php include "JS/Head.js" ?>
    </script>
</header>

<main>
    <i class="far fa-heart fa-2x"></i>
    <h2 class="warning">You have no Saved Iteams</h2>
    <br>
    <h3 class="orienter">Sign In to Sync your Saved Iteams <br> across all your devices.</h3>
    <br>
    <a href="Login.php"><input type="button" class="signInButton" value="SIGN IN"></a>
</main>

<footer>
    <script src="JS/Footer.js"></script>
</footer>
</body>
</html>