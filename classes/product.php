<?php
require_once('models/dbConnection.php');

class product
{
    private $prod_id;
    private $description;
    private $price;
    private $brands;
    private $category;

    public function __construct($prod_id,$description,$price,$brands,$category)
    {
        $this->prod_id=$prod_id;
        $this->description=$description;
        $this->price=$price;
        $this->brands=$brands;
        $this->category=$category;
    }

    public function getProdId()
    {
        return $this->prod_id;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function getBrands()
    {
        return $this->brands;
    }

    public function setBrands($brands)
    {
        $this->brands = $brands;
    }
    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory($category)
    {
        $this->category = $category;
    }

    public function getAll()
    {
        $query = "select * from product";
        $products = $this->getConnection($query);
        return $products;
    }

    public function getProducts($id)
    {
        $products = (array)null;
        if(!empty($id)) {
            $query = "select * from product where prod_id='$id'";
            foreach ($this->getConnection($query) as $produkti)
            {
                $prod = new product($produkti['prod_id'], $produkti['description'],$produkti['price'],$produkti['brands'],$produkti['category']);
                array_push($products,$prod);
            }
        }
        return $products;
    }

    public function getProductsByGender($gender)
    {
        $products = (array)null;
        if(!empty($gender)) {
            $query = "select * from product where category='$gender'";
            foreach ($this->getConnection($query) as $produkti)
            {
                $prod = new product($produkti['prod_id'], $produkti['description'],$produkti['price'],$produkti['brands'],$produkti['category']);
                array_push($products,$prod);
            }
        }
        return $products;
    }

    public function addProduct($product)
    {
        if(!is_null($product))
        {
            $desc = $product->getDescription();
            $pric = $product->getPrice();
            $brand = $product->getBrands();
            $cate = $product->getCategory();

            $dbcon = new DbConnection();
            $connection = $dbcon->getConnection();
            $query = $connection->prepare("insert into product (description,price,brands,category) values ( :desc , :pric, :brand, :cate)");
            $query->bindParam(':desc',$desc);
            $query->bindParam(':pric',$pric);
            $query->bindParam(':brand',$brand);
            $query->bindParam(':cate',$cate);
            $query->execute();
            $query->closeCursor();
        }
    }

    public function updateProduct($product)
    {
        if(!is_null($product)){
            $id = $product->getProdId();
            $desc = $product->getDescription();
            $pric = $product->getPrice();
            $brand = $product->getBrands();
            $cate = $product->getCategory();
            $dbcon = new DbConnection();
            $connection = $dbcon->getConnection();
            $query = $connection->prepare ("update product set description=:desc, price=:pric, brands=:brand, category=:cate where prod_id=:id");
            $query->bindParam(':id',$id);
            $query->bindParam(':desc',$desc);
            $query->bindParam(':pric',$pric);
            $query->bindParam(':brand',$brand);
            $query->bindParam(':cate',$cate);
            $query->execute();
            $query->closeCursor();
        }
    }

    public function deleteProduct($id)
    {
        if(!is_null($id)){
            $query = "delete from product where prod_id='$id'";
            $this->getConnectionWithoutArray($query);
        }
    }

    public function getConnectionWithoutArray($query)
    {
        $dbcon = new DbConnection();
        $connection = $dbcon->getConnection();
        try {
            $product_statement = $connection->prepare($query);
            $product_statement->execute();
            $product_statement->closeCursor();
        }catch (PDOException $pdo)
        {
            die("Error could not be updated");
        }
    }

    public function getConnection($query)
    {
        $dbcon = new DbConnection();
        $connection = $dbcon->getConnection();
        try {
            $product_statement = $connection->prepare($query);
            $product_statement->execute();
            $products = $product_statement->fetchAll();
            $product_statement->closeCursor();
            return $products;
        }catch (PDOException $pdo)
        {
            die("Error could not be updated");
        }
    }
}
