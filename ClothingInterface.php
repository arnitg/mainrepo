<?php
require_once('models/dbConnection.php');
require_once('classes/product.php');
require_once('classes/photos.php');

if(isset($_GET['category'])) {
    $jk = $_GET['category'];
    $pictures = new photos(null,null,null);
    $product = new product(null,null,null,null,null);
    $tekstet =$product->getProductsByGender($jk);
    $titulli =  $tekstet[0]->getDescription();
    $price = $tekstet[0]->getPrice();
    $brand = $tekstet[0]->getBrands();
    $category = $tekstet[0]->getCategory();
    $fotot = $pictures->getPictures(1);

    $paths = (array)null;
    $ids = (array)null;
    for($j = 0; $j<count($fotot);$j++) {
        if(!(in_array($fotot[$j]->getProdId(), $ids))) {
            array_push($paths, $fotot[$j]->getPath());
            array_push($ids, $fotot[$j]->getProdId());
        }
    }
} else {
    echo 'ska id tnjejt' . $_GET['category'];
};

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo $category.'Clothing'?> • Crocodile&trade;</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://kit.fontawesome.com/572ab64f26.js" crossorigin="anonymous"></script>
    <link rel="icon" href="images/favicon.png"/>
    <link rel="stylesheet" href="CSS/Opening.css">
    <link rel="stylesheet" href="CSS/MainCss.css">
    <link rel="stylesheet" href="CSS/ClothingInterfaceCss.css">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <script src="JS/ClothingJS.js"></script>
    <script src="JS/Throbber.js"></script>
    <script src="JS/toTopButton.js"></script>
    <link rel="stylesheet" href="CSS/toTopButton.css">
</head>
<body>
    <script>
        <?php include "JS/Head.js" ?>
    </script>
       <main class="mainClothesSection" id ="mainClothesSection">
           <div class="typeOf">
               <h1><?php echo $category?> Clothing</h1>
           </div>
           <div class="clothes" id="clothes">
               <div class="clothes-row">
                   <?php for ($i = 0;$i<count($tekstet); $i++):?>
                   <article class="photo-text">
                       <a class="photo" href="BuyInterface.php?prod_id=1" style="background-image: url(<?php echo 'images/' . $paths[0]?>)"></a>
                       <div class="text">
                           <a href="BuyInterface.php?prod_id=1" class="description"><?php echo $tekstet[$i]->getDescription()?></a>
                           <p class="price"><?php echo $tekstet[$i]->getPrice()?>$</p>
                       </div>
                   </article>
                   <?php endfor;?>

                   <article class="photo-text">
                       <a class="photo" href="BuyInterface.php?prod_id=3" style="background-image: url(images/Ralph.jpg)"></a>
                       <div class="text">
                           <a href="BuyInterface.php?prod_id=3" class="description">White shirt,new design,summer,skrr</a>
                           <p class="price">100$</p>
                       </div>
                   </article>
<!--                   <article class="photo-text">-->
<!--                       <a class="photo" href="BuyInterface.php?prod_id=4" style="background-image: url(images/Ralph.jpg)"></a>-->
<!--                       <div class="text">-->
<!--                           <a href="BuyInterface.php?prod_id=4" class="description">White shirt,new design,summer,skrr</a>-->
<!--                           <p class="price">100$</p>-->
<!--                       </div>-->
<!--                   </article>-->
               </div>
               <div class="clothes-row">
                   <article class="photo-text">
                       <a class="photo" href="BuyInterface.php?prod_id=5" style="background-image: url(images/Ralph.jpg)"></a>
                       <div class="text">
                           <a href="BuyInterface.php?prod_id=5" class="description">White shirt,new design,summer,skrr</a>
                           <p class="price">100$</p>
                       </div>
                   </article>
                   <article class="photo-text">
                       <a class="photo" href="BuyInterface.php?prod_id=6" style="background-image: url(images/Ralph.jpg)"></a>
                       <div class="text">
                           <a href="BuyInterface.php?prod_id=6" class="description">White shirt,new design,summer,skrr</a>
                           <p class="price">100$</p>
                       </div>
                   </article>
                   <article class="photo-text">
                       <a class="photo" href="BuyInterface.php?prod_id=1" style="background-image: url(images/Ralph.jpg)"></a>
                       <div class="text">
                           <a href="BuyInterface.php?prod_id=1" class="description">White shirt,new design,summer,skrr</a>
                           <p class="price">100$</p>
                       </div>
                   </article>
                   <article class="photo-text">
                       <a class="photo" href="BuyInterface.php?prod_id=1" style="background-image: url(images/Ralph.jpg)"></a>
                       <div class="text">
                           <a href="BuyInterface.php?prod_id=1" class="description">White shirt,new design,summer,skrr</a>
                           <p class="price">100$</p>
                       </div>
                   </article>
               </div>
               <div class="clothes-row">
                   <article class="photo-text">
                       <a class="photo" href="BuyInterface.php?prod_id=1" style="background-image: url(images/Ralph.jpg)"></a>
                       <div class="text">
                           <a href="BuyInterface.php?prod_id=1" class="description">White shirt,new design,summer,skrr</a>
                           <p class="price">100$</p>
                       </div>
                   </article>
                   <article class="photo-text">
                       <a class="photo" href="BuyInterface.php?prod_id=1" style="background-image: url(images/Ralph.jpg)"></a>
                       <div class="text">
                           <a href="BuyInterface.php?prod_id=1" class="description">White shirt,new design,summer,skrr</a>
                           <p class="price">100$</p>
                       </div>
                   </article>
                   <article class="photo-text">
                       <a class="photo" href="BuyInterface.php?prod_id=1" style="background-image: url(images/Ralph.jpg)"></a>
                       <div class="text">
                           <a href="BuyInterface.php?prod_id=1" class="description">White shirt,new design,summer,skrr</a>
                           <p class="price">100$</p>
                       </div>
                   </article>
                   <article class="photo-text">
                       <a class="photo" href="BuyInterface.php?prod_id=1" style="background-image: url(images/Ralph.jpg)"></a>
                       <div class="text">
                           <a href="BuyInterface.php?prod_id=1" class="description">White shirt,new design,summer,skrr</a>
                           <p class="price">100$</p>
                       </div>
                   </article>
               </div>
               <div class="clothes-row">
                   <article class="photo-text">
                       <a class="photo" href="BuyInterface.php?prod_id=1" style="background-image: url(images/Ralph.jpg)"></a>
                       <div class="text">
                           <a href="BuyInterface.php?prod_id=1" class="description">White shirt,new design,summer,skrr</a>
                           <p class="price">100$</p>
                       </div>
                   </article>
                   <article class="photo-text">
                       <a class="photo" href="BuyInterface.php?prod_id=1" style="background-image: url(images/Ralph.jpg)"></a>
                       <div class="text">
                           <a href="BuyInterface.php?prod_id=1" class="description">White shirt,new design,summer,skrr</a>
                           <p class="price">100$</p>
                       </div>
                   </article>
                   <article class="photo-text">
                       <a class="photo" href="BuyInterface.php?prod_id=1" style="background-image: url(images/Ralph.jpg)"></a>
                       <div class="text">
                           <a href="BuyInterface.php?prod_id=1" class="description">White shirt,new design,summer,skrr</a>
                           <p class="price">100$</p>
                       </div>
                   </article>
                   <article class="photo-text">
                       <a class="photo" href="BuyInterface.php?prod_id=1" style="background-image: url(images/Ralph.jpg)"></a>
                       <div class="text">
                           <a href="BuyInterface.php?prod_id=1" class="description">White shirt,new design,summer,skrr</a>
                           <p class="price">100$</p>
                       </div>
                   </article>
               </div>
           </div>
       </main>
       <div class="ViewMoreButton">
           <p class="opening">Want to check out more</p>
           <button class="Buttons" onclick="moreClothes()">More Clothes</button>
       </div>
       <script src="JS/Footer.js"></script>
</body>
</html>
