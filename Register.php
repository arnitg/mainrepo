<?php
require './controllers/UserController.php';
require 'controllers/ValidationController.php';

$errors = [];

if(isset($_POST['submit'])){

    $user = new UserController;
    $validation = new Validation($_POST);
    $errors = $validation->validateForm();

    $firstname = htmlspecialchars($_POST['Firstname']);
    $lastname = htmlspecialchars($_POST['Lastname']);
    $email = htmlspecialchars($_POST['Email']);
    $password = htmlspecialchars(($_POST['Password']));

    if(sizeof($errors) == 0){
        $user->store($_POST);
    }
}

?>

<!DOCTYPE html>
<html>
<head>
    <title>Join Us • Crocodile&trade;</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="icon" href="images/favicon.png"/>
    <script src="https://kit.fontawesome.com/572ab64f26.js" crossorigin="anonymous"></script>
    <script src="JS/Login&Register.js"></script>
    <style>
        <?php include 'CSS/Login&Register.css'?>
    </style>
</head>
<body>
<header>
    <h1>CROCODILE&trade;</h1>
</header>
<main>
    <div class="register">
        <div class="registerOrLogin" id="newToCrocodile">
            <a href="Register.php">
                NEW TO CROCODILE?
            </a>
        </div>

        <div class="registerOrLogin" id="alreadyRegistered">
            <a href="Login.php">
                ALREADY REGISTERED?
            </a>
        </div>

        <h3>
            SIGN UP WITH ...
        </h3>

        <button class="signInWith" onclick="MyWindow=window.open('https://www.facebook.com/login/','MyWindow','width=600,height=650');">
            <img src="images/facebook.png" alt="facebook">
            <h3>Facebook</h3>
        </button>

        <button class="signInWith" onclick="MyWindow=window.open('https://accounts.google.com/signin','MyWindow','width=600,height=650');">
            <img src="images/google.png" alt="google">
            <h3>Google</h3>
        </button>

        <button class="signInWith" onclick="MyWindow=window.open('https://twitter.com/login' ,'MyWindow','width=600,height=650');">
            <img src="images/Twitter.png" alt="twitter">
            <h3>Twitter</h3>
        </button>
        <div class="hrdivider">
            <hr/>
            <span>OR</span>
        </div>

        <h3 class="EAsignUp">
            SIGN UP USING YOUR EMAIL ADDRESS
        </h3>

        <div class="form">
            <form action="" name="registerForm" method="POST">
                <!--                        onsubmit="return validateDay() || validateMonth() || validateYear() || validateOption() || validateFirstname() || validateLastname() || validateEmail() || validatePassword() || validatePasswordConfirmation() "-->
                <label for="Firstname">Firstname:</label><br>
                <input class="personalInput" type="text" name="Firstname" id="Firstname"  onchange="return validateFirstname()" value="<?php echo $firstname ?? '' ?>"><br>
                <p class="registerProblem" id="firstnameProblem"><?php echo $errors['Firstname'] ?? '' ?></p>
                <label for="Lastname">Lastname:</label><br>
                <input class="personalInput" type="text" name="Lastname" id="Lastname"  onchange="return validateLastname()" value="<?php echo $lastname ?? '' ?>"><br>
                <p class="registerProblem" id="lastnameProblem"><?php echo $errors['Lastname'] ?? '' ?></p>
                <label for="Email">Email:</label><br>
                <input class="personalInput" type="email" name="Email" id="Email"   onchange="return validateEmail()" value="<?php echo $email ?? '' ?>"><br>
                <p class="registerProblem" id="emailProblem"><?php echo $errors['Email'] ?? '' ?></p>
                <label for="Password">Password:</label><br>
                <input class="personalInput" type="password" name="Password" id="Password"   onchange="return validatePassword()" value="<?php echo $password ?? '' ?>"><br>
                <p class="registerProblem" id="passwordProblem"><?php echo $errors['Password'] ?? '' ?></p>
                <label for="PasswordConfirmation">Confirm Password:</label><br>
                <input class="personalInput" type="password" name="PasswordConfirmation" id="PasswordConfirmation"   onchange="return validatePasswordConfirmation()"><br>
                <p class="registerProblem" id="passwordConfirmationProblem"><?php echo $errors['PasswordConfirmation'] ?? '' ?></p>
                <label>Date Of Birth:</label><br>
                <select class="selectInput" id="day" name="BirthDay"  onchange="return validateDay()">
                    <option value="day" selected>Day</option>
                    <option>01</option>
                    <option>02</option>
                    <option>03</option>
                    <option>04</option>
                    <option>05</option>
                    <option>06</option>
                    <option>07</option>
                    <option>08</option>
                    <option>09</option>
                    <option>10</option>
                    <option>11</option>
                    <option>12</option>
                    <option>13</option>
                    <option>14</option>
                    <option>15</option>
                    <option>16</option>
                    <option>17</option>
                    <option>18</option>
                    <option>19</option>
                    <option>20</option>
                    <option>21</option>
                    <option>22</option>
                    <option>23</option>
                    <option>24</option>
                    <option>25</option>
                    <option>26</option>
                    <option>27</option>
                    <option>28</option>
                    <option>29</option>
                    <option>30</option>
                    <option>31</option>
                </select>
                <select class="selectInput" id="month" name="BirthMonth"  onchange="return validateMonth()">
                    <option value="month" selected>Month</option>
                    <option>January</option>
                    <option>February</option>
                    <option>March</option>
                    <option>April</option>
                    <option>May</option>
                    <option>June</option>
                    <option>July</option>
                    <option>August</option>
                    <option>September</option>
                    <option>October</option>
                    <option>November</option>
                    <option>December</option>
                </select>
                <select class="selectInput" id="year" name="BirthYear"  onchange="return validateYear()">
                    <option value="year" selected>Year</option>
                    <option value="2018">2018</option>
                    <option value="2017">2017</option>
                    <option value="2016">2016</option>
                    <option value="2015">2015</option>
                    <option value="2014">2014</option>
                    <option value="2013">2013</option>
                    <option value="2012">2012</option>
                    <option value="2011">2011</option>
                    <option value="2010">2010</option>
                    <option value="2009">2009</option>
                    <option value="2008">2008</option>
                    <option value="2007">2007</option>
                    <option value="2006">2006</option>
                    <option value="2005">2005</option>
                    <option value="2004">2004</option>
                    <option value="2003">2003</option>
                    <option value="2002">2002</option>
                    <option value="2001">2001</option>
                    <option value="2000">2000</option>
                    <option value="1999">1999</option>
                    <option value="1998">1998</option>
                    <option value="1997">1997</option>
                    <option value="1996">1996</option>
                    <option value="1995">1995</option>
                    <option value="1994">1994</option>
                    <option value="1993">1993</option>
                    <option value="1992">1992</option>
                    <option value="1991">1991</option>
                    <option value="1990">1990</option>
                    <option value="1989">1989</option>
                    <option value="1988">1988</option>
                    <option value="1987">1987</option>
                    <option value="1986">1986</option>
                    <option value="1985">1985</option>
                    <option value="1984">1984</option>
                    <option value="1983">1983</option>
                    <option value="1982">1982</option>
                    <option value="1981">1981</option>
                    <option value="1980">1980</option>
                    <option value="1979">1979</option>
                    <option value="1978">1978</option>
                    <option value="1977">1977</option>
                    <option value="1976">1976</option>
                    <option value="1975">1975</option>
                    <option value="1974">1974</option>
                    <option value="1973">1973</option>
                    <option value="1972">1972</option>
                    <option value="1971">1971</option>
                    <option value="1970">1970</option>
                    <option value="1969">1969</option>
                    <option value="1968">1968</option>
                    <option value="1967">1967</option>
                    <option value="1966">1966</option>
                    <option value="1965">1965</option>
                    <option value="1964">1964</option>
                    <option value="1963">1963</option>
                    <option value="1962">1962</option>
                    <option value="1961">1961</option>
                    <option value="1960">1960</option>
                    <option value="1959">1959</option>
                    <option value="1958">1958</option>
                    <option value="1957">1957</option>
                    <option value="1956">1956</option>
                    <option value="1955">1955</option>
                    <option value="1954">1954</option>
                    <option value="1953">1953</option>
                    <option value="1952">1952</option>
                    <option value="1951">1951</option>
                    <option value="1950">1950</option>
                    <option value="1949">1949</option>
                    <option value="1948">1948</option>
                    <option value="1947">1947</option>
                    <option value="1946">1946</option>
                    <option value="1945">1945</option>
                    <option value="1944">1944</option>
                    <option value="1943">1943</option>
                    <option value="1942">1942</option>
                    <option value="1941">1941</option>
                    <option value="1940">1940</option>
                    <option value="1939">1939</option>
                    <option value="1938">1938</option>
                    <option value="1937">1937</option>
                    <option value="1936">1936</option>
                    <option value="1935">1935</option>
                    <option value="1934">1934</option>
                    <option value="1933">1933</option>
                    <option value="1932">1932</option>
                    <option value="1931">1931</option>
                    <option value="1930">1930</option>
                    <option value="1929">1929</option>
                    <option value="1928">1928</option>
                    <option value="1927">1927</option>
                    <option value="1926">1926</option>
                    <option value="1925">1925</option>
                    <option value="1924">1924</option>
                    <option value="1923">1923</option>
                    <option value="1922">1922</option>
                    <option value="1921">1921</option>
                    <option value="1920">1920</option>
                    <option value="1919">1919</option>
                    <option value="1918">1918</option>
                    <option value="1917">1917</option>
                    <option value="1916">1916</option>
                    <option value="1915">1915</option>
                    <option value="1914">1914</option>
                    <option value="1913">1913</option>
                    <option value="1912">1912</option>
                    <option value="1911">1911</option>
                    <option value="1910">1910</option>
                    <option value="1909">1909</option>
                    <option value="1908">1908</option>
                    <option value="1907">1907</option>
                    <option value="1906">1906</option>
                    <option value="1905">1905</option>
                </select>
                <br>
                <div class="birthDateOption">
                    <p class="dayProblem" id="dayMissing"><?php echo $errors['BirthDay'] ?? '' ?></p>
                    <p class="monthProblem" id="monthMissing"></p>
                    <p class="yearProblem" id="yearMissing"></p>
                </div>
                <br>
                <label>You are Interested in:</label><br><br>
                <input type="radio" name="InterestedIn" class="wearInput" id="Womenswear" value="Womenswear">Womenswear
                <input type="radio" name="InterestedIn" class="wearInput" id="Menswear" value="Menswear">Menswear
                <p class="registerProblemOptions" id="optionProblem">
                    <?php echo $errors['InterestedIn'] ?? '' ?>
                </p>
                <br><br>
                <p class="terms">
                    By creating your account, you agree to our
                    <a href=""  class="privacyPolicy">Terms and Conditions</a> & <a href=""  class="privacyPolicy">Privacy Policy</a>
                </p>

                <input type="submit" name="submit" value="JOIN CROCODILE" class="submitbutton">
            </form>
        </div>
    </div>
</main>
</body>
</html>