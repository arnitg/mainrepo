<?php

class DbConnection{
    private $serverName = "localhost";
    private $databaseName = "crocodiledb";
    private $user = "root";
    private $password = "";

    public $pdo;

    public function __construct()
    {
        try{
//            session_start();
            $link = new PDO('mysql:host=localhost;dbname=crocodiledb', "root", "");
            $this->pdo = $link;
        }catch(PDOException $e) {
            die('DIE'. $e->getMessage());
        }
    }

    public function getConnection(){

        try{
            $connection = new PDO("mysql:host=$this->serverName;dbname=$this->databaseName;charset=utf8mb4", $this->user, $this->password);

            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch(PDOException $e){
            echo $e->getMessage();
        }

        return $connection;
    }
}
