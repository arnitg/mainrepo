<?php


class photos
{
    public function __construct($photo_id,$path,$prod_id)
    {
        $this->photo_id=$photo_id;
        $this->path=$path;
        $this->prod_id=$prod_id;
    }

    public function getPhotoId()
    {
        return $this->photo_id;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function setPath($path)
    {
        $this->path = $path;
    }

    public function getProdId()
    {
        return $this->prod_id;
    }

    public function setProdId($prod_id)
    {
        $this->prod_id=$prod_id;
    }

    public function getAll()
    {
        $query = "select * from photos";
        $photos = $this->getConnection($query);
        return $photos;
    }

    public function getPictures($id)
    {
        $pictures = (array)null;
        if(!empty($id)) {
            $query = "select * from photos pp where pp.prod_id = '$id' order by pp.photo_id";
            foreach ($this->getConnection($query) as $foto)
            {
                $pic = new photos($foto['photo_id'],$foto['path'],$foto['prod_id']);
                array_push($pictures,$pic);
            }
        }
        return $pictures;
    }

    public function addPictures($photos)
    {
        if(!is_null($photos))
        {
            $pth = $photos->getPath();
            $pid = $photos->getProdId();
            $dbcon = new DbConnection();
            $connection = $dbcon->getConnection();
            $query =$connection->prepare("insert into photos (path,prod_id) values (:pth,:pid)");
            $query->bindParam(':pth',$pth);
            $query->bindParam(':pid',$pid);
            $query->execute();
            $query->closeCursor();
        }
    }

    public function updatePictures($photos)
    {
        if(!is_null($photos))
        {
            $id = $photos->getPhotoId();
            $pth = $photos->getPath();
            $pid = $photos->getProdId();
            $dbcon = new DbConnection();
            $connection = $dbcon->getConnection();
            $query = $connection->prepare ("update photos set prod_id=:pid,path=:pth where photo_id=:id");
            $query->bindParam(':id',$id);
            $query->bindParam(':pth',$pth);
            $query->bindParam(':pid',$pid);
            $query->execute();
            $query->closeCursor();
        }
    }

    public function deletePictures($id)
    {
        if(!is_null($id))
        {
            $query = "delete from photos where photo_id='$id'";
            $this->getConnectionWithoutArray($query);
        }
    }

    public function getConnectionWithoutArray($query)
    {
        $dbcon = new DbConnection();
        $connection = $dbcon->getConnection();
        try {
            $product_statement = $connection->prepare($query);
            $product_statement->execute();
            $product_statement->closeCursor();
        }catch (PDOException $pdo)
        {
            die("Error could not be updated");
        }
    }

    public function getConnection($query)
    {
        $dbcon = new DbConnection();
        $connection = $dbcon->getConnection();
        $product_statement = $connection->prepare($query);
        $product_statement->execute();
        $products = $product_statement->fetchAll();
        $product_statement->closeCursor();
        return $products;
    }
}
