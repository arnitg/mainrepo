<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>About • Crocodile&trade;</title>
        <link rel="stylesheet" href="CSS/Opening.css">
        <link rel="stylesheet" href="CSS/MainCss.css">
        <link rel="icon" href="images/favicon.png"/>
        <script src="JS/Throbber.js"></script>
        <script src="JS/OpeningJS.js"></script>
        <style>
            <?php include 'CSS/AboutUS.css'?>
        </style>
    </head>
    <body>
        <div class="about">
            <ul>
                <li>Who we are</li>
                <li class="second">The biggest fashion store</li>
                <li ><button onclick="go('ourstory')" class="Buttons">Read the Crocodile&trade;</button></li>
            </ul>
        </div>
        <div class="brands">
            <ul>
                <li>Our Supporters</li>
                <li class="second">The best brands out there</li>
                <li><button onclick="go('brands')" class="Buttons">Our Brands</button></li>
            </ul>
        </div>
    </body>
</html>
