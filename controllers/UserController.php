<?php

include './models/dbConnection.php';

class UserController
{
    protected $db;

    public function __construct()
    {
        $this->db = new DbConnection();
    }

    public function all()
    {
        $query = $this->db->pdo->query('SELECT * FROM users');

        return $query->fetchAll();
    }

    public function findByEmail($Email) {
        $query = $this->db->pdo->prepare('select * from users where Email = :Email');
        $query->execute(['Email' => $Email]);

        return $query->fetchAll();
    }

    public function store($request)
    {
        // isset($request['is_admin']) ? $isAdmin = 1 : $isAdmin = 0;
        $Password = password_hash($request['Password'], PASSWORD_BCRYPT);
        $PasswordConfirmation = password_hash($request['PasswordConfirmation'], PASSWORD_BCRYPT);

        $query = $this->db->pdo->prepare("insert into users (Firstname, Lastname, Email, Password, PasswordConfirmation, BirthDay, BirthMonth, BirthYear, InterestedIn) values (:Firstname, :Lastname, :Email, :Password, :PasswordConfirmation, :BirthDay, :BirthMonth, :BirthYear, :InterestedIn)");

        $query->bindParam(':Firstname', $request['Firstname']);
        $query->bindParam(':Lastname', $request['Lastname']);
        $query->bindParam(':Email', $request['Email']);
        $query->bindParam(':Password', $Password);
        $query->bindParam(':PasswordConfirmation', $PasswordConfirmation);
        $query->bindParam(':BirthDay', $request['BirthDay']);
        $query->bindParam(':BirthMonth', $request['BirthMonth']);
        $query->bindParam(':BirthYear', $request['BirthYear']);
        $query->bindParam(':InterestedIn', $request['InterestedIn']);
        // $query->bindParam(':is_admin', $isAdmin);
        $query->execute();

        return header('Location: http://localhost/mainrepo/Login.php');


    }


}
