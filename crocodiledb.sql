-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 15, 2020 at 11:46 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crocodiledb`
--

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE `photos` (
  `photo_id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `prod_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`photo_id`, `path`, `prod_id`) VALUES
(1, 'women-sitting-close-together-4557402.jpg', 1),
(2, 'red-and-white-samsung-logo-4108125.jpg', 1),
(3, 'aerial-view-of-cars-parked-on-parking-lot-4328961.jpg', 1),
(4, 'man-in-white-dress-shirt-holding-black-smartphone-4006567.jpg', 1),
(5, 'man-in-white-hoodie-covering-his-face-with-white-flower-4477735.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `prod_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `brands` varchar(100) NOT NULL,
  `category` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`prod_id`, `description`, `price`, `brands`, `category`) VALUES
(1, 'White shirt,new design,summer,skrr', 100, 'Lacoste', 'Men'),
(2, 'White shirt,new design,summer,skrr1', 300, 'Fendi', 'Men'),
(3, 'White shirt,new design,summer,skrr2', 205, 'Zara', 'Men'),
(4, 'ASOS DESIGN slim fit racer front top in black', 1002, 'Polo', 'Women'),
(5, 'ASOS DESIGN slim fit racer front top in black', 1030, 'Thommy Hilfiger', 'Women'),
(6, 'ASOS DESIGN slim fit racer front top in black', 100000, 'BALENCIAGA', 'Women');

-- --------------------------------------------------------

--
-- Table structure for table `tblcontent`
--

CREATE TABLE `tblcontent` (
  `id` int(11) NOT NULL,
  `page` varchar(255) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `text` varchar(700) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tblcontent`
--

INSERT INTO `tblcontent` (`id`, `page`, `title`, `image`, `text`) VALUES
(1, 'ourstory', 'CONNECTED.', '1250184.jpg', 'test text'),
(13, 'ourstory', ' SHIFT. ', 'truckgirl.jpg', 'Crocodile&trade;, took it\'s iconic logo from the most unearthly animal, the crocodile. <br> It\'s power to shift from water to earth in just a second <br> power, beauty and the name it has made us who we are. <br> we try to put together anyone and everyone for the best reflection of our society <br> the future and the past directly impact our present, we put our selves one step further.<br>'),
(14, 'ourstory', ' ACCELERATED. ', '', 'Earth is changing day by day, and with that<br> our team is trying to bring the most to our costumers<br> we try to put together the lastest fashion to make the world above and beyond our sightings<br> Eclectic, contemporary, romantic—crocodile&trade; products represent the pinnacle of Italian craftsmanship <br>and are unsurpassed for their quality and attention to detail. So we make sure everyone has an equal chance to discover all the amazing things they’re capable of – <br> no matter who they are, where they’re from or what looks they like to boss. <br>'),
(15, 'ourstory', ' GARMENT. ', 'Prada-x-Linea-x-Rossa-x-exclusive-x-article.jpg', 'Nurturing a highly intimate relationship with its customers,<br> our team is trying to bring the most to our costumers<br> It’s super-important for us to promote a healthy body image – we’re not about conforming to any stereotypes <br> – so we work with more than 200 models to represent our audience.<br> our clothing require attention and love and we offer both of those.'),
(16, 'ourstory', ' MODERN. ', 'Balenciaga-Fall-Winter-2019-Campaign-003.jpg', 'Designing clothes is not our view <br> we try to focus on bringing high-end brands and costumers closer than ever <br>\r\nFuturistic and oblique fashion is our main view.'),
(17, 'ourstory', ' REACH. ', '', ' We try to reach corners and high-end peaks <br> bringing costumers in the spotlight and trying to put satisfaction our main focus <br>...'),
(18, 'ourstory', ' CONTACT! ', 'girls.jpg', 'crocodile&trade;<br/>  Shibuya City, Tōkyō-to,Japan<br/> 150-0041<br/> contact@crocodile.com<br/> Crisp Fashion Limited<br/>Registration No.44553130013<br/> VAT No. J455 699 32 Press information: info@crocodile.com'),
(19, 'brands', 'Crocodile Brands', 'sea.jpeg', 'Check out brands like'),
(20, 'brands', 'A site of the future', 'brands.jpg', 'When building a brand, it’s not enough to create a great product or service and call it a day. If you want your brand to have any sort of longevity, you need to build strong relationships with people—and that requires something more: transparency. People want to see your brand as a whole entity. They want to know not only what you do but what your brand values and believes in.\r\nCrocodile is as site of many brands we are a site of the future.'),
(21, 'brands', 'Our Protection Policies', 'city.jpeg', 'The BBBBBB heritage is that of René Lacoste, the legendary tennis champion who created the BBBBBB polo. His celebrated shirt in petit pique would revolutionize the worlds of sport and fashion, giving rise to a brand. The BBBBBB heritage is that of René Lacoste, the legendary tennis champion who created the BBBBBB polo. His celebrated shirt in petit pique would revolutionize the worlds of sport and fashion, giving rise to a brand. His celebrated shirt in petit pique would revolutionize the worlds of sport and fashion, giving rise to a brand.'),
(22, 'brands', 'RENÉ LACOSTE', '', '<br />Confidence, tenacity, perseverance and rigor: <br />these are the tools I believe necessary to find one’s way in life. <br />'),
(23, 'main', 'Marc Jacobs', 'slide1.jpg', 'Clothes mean nothing until someone lives in them.'),
(24, 'main', 'Kenzo Takada', 'slide2.jpg', 'Fashion is like eating, you shouldn\'t stick to the same menu.'),
(25, 'main', 'John Galliano', 'head.jpg', 'The joy of dressing is an art.'),
(26, 'main', 'Men Clothing', 'brands.jpg', ''),
(27, 'main', 'Women Clothing', 'brands.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `text`
--

CREATE TABLE `text` (
  `text_id` int(10) UNSIGNED NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `text`
--

INSERT INTO `text` (`text_id`, `description`) VALUES
(1, 'test tekst');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `ID` int(50) NOT NULL,
  `Firstname` varchar(50) NOT NULL,
  `Lastname` varchar(50) NOT NULL,
  `Email` varchar(150) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `PasswordConfirmation` varchar(255) NOT NULL,
  `BirthDay` varchar(20) NOT NULL,
  `BirthMonth` varchar(20) NOT NULL,
  `BirthYear` varchar(20) NOT NULL,
  `InterestedIn` varchar(20) DEFAULT NULL,
  `Admin` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID`, `Firstname`, `Lastname`, `Email`, `Password`, `PasswordConfirmation`, `BirthDay`, `BirthMonth`, `BirthYear`, `InterestedIn`, `Admin`) VALUES
(1, 'Denis', 'Bajcinca', 'db43784@ubt-uni.net', '$2y$10$8Ox1BJ3PFsBoEv/dryL1u.PmnekIgRHwEcg66bBcEwyoWX2wl96wi', '$2y$10$hwV.syn3agndkKwW1WbmHOHmTP52Kr4z30dDufKsfsVCqYlQp9c9i', '25', 'May', '2000', 'Menswear', b'1'),
(2, 'Meris', 'Nici', 'mn43846@ubt-uni.net', '$2y$10$5Zghnrc2V/liJ5ueVtKR0e3ltvAc6z0zal.Z8FwO64U.QKSgEfsu.', '$2y$10$Gab7LZNaBcjRfc0md75fjeD9S94bCg56G7lyiV7cfpsEK9Q59J6TO', '02', 'September', '2000', 'Menswear', NULL),
(3, 'Donart', 'Ramadani', 'dr43950@ubt-uni.net', '$2y$10$OmnvbacYw9CVOgBrwKUey.lOM2o6oQvFlP5lZ/F4fGWbHzwD4hZ4O', '$2y$10$tDjRkkY3tlD0VdKBv4IMM.LEYQYgzpebHWdpI.xLyxD2eE3OgRAFe', '20', 'August', '2000', 'Womenswear', b'1'),
(4, 'Arnit', 'Gashi', 'ag43420@ubt-uni.net', '$2y$10$kwIBEk/GBPSKdccgL5OZ/uCVS38K/uDS28FKPvpWr4G5kIznNxz.O', '$2y$10$DbU9RgHbMp1HeMDCXCu9Xec3jheK5nLKd6nmCo3sQyD.DsVzS/tpK', '04', 'June', '2001', 'Womenswear', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`photo_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`prod_id`);

--
-- Indexes for table `tblcontent`
--
ALTER TABLE `tblcontent`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `text`
--
ALTER TABLE `text`
  ADD PRIMARY KEY (`text_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `photos`
--
ALTER TABLE `photos`
  MODIFY `photo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `prod_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tblcontent`
--
ALTER TABLE `tblcontent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
