const carsuelSlide = document.querySelector(".carousel-slide");
const carouselImages = document.querySelectorAll(".carousel-slide img");

const prevBtn = document.querySelector("#prevBtn");
const nextBtn = document.querySelector("#nextBtn");

let counter = 1;
const size = carouselImages[0].clientWidth;

carsuelSlide.style.transform = "translateX(" +(-size * counter) + "px)";

nextBtn.addEventListener("click",()=>{
    carsuelSlide.style.transition = "transform 0.4s ease-in-out";
    counter++;
    carsuelSlide.style.transform = "translateX(" +(-size * counter) + "px)";
});
prevBtn.addEventListener("click",()=>{
    carsuelSlide.style.transition = "transform 0.4s ease-in-out";
    counter--;
    carsuelSlide.style.transform = "translateX(" +(-size * counter) + "px)";
});

carsuelSlide.addEventListener("transitionend", () => {
    if(carouselImages[counter].id === "lastClone") {
       carsuelSlide.style.transition = "none";
       counter = carouselImages.length - 2;
       carsuelSlide.style.transform = "translateX(" +(-size * counter) + "px)";
    }
    if(carouselImages[counter].id === "firstClone") {
        carsuelSlide.style.transition = "none";
        counter = carouselImages.length - counter;
        carsuelSlide.style.transform = "translateX(" +(-size * counter) + "px)";
    }
});