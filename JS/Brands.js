
var c = 3;
var bannerStatus = 1;
var bannerTimer = 7000;
window.onload = function () {
    bannerLoop();
};
var startBannerLoop = setInterval(function () {
    bannerLoop();
}, bannerTimer);
function stop() {
    clearInterval(startBannerLoop);
}
function play() {
    startBannerLoop = setInterval(function () {
        bannerLoop();
    }, bannerTimer);
}
function next() {
    bannerLoop();
}
function prev() {
    if(bannerStatus === 1) {
        bannerStatus = 2;
    }
    else if(bannerStatus === 2) {
        bannerStatus = 3;
    }
    else if(bannerStatus === 3) {
        bannerStatus = 1;
    }
    bannerLoop2();
}

function bannerLoop() {
    if (bannerStatus === 1) {
        document.getElementById("imgban3").style.transition = "3s";
        document.getElementById("imgban3").style.opacity = "0";
        document.getElementById("imgban1").style.opacity = "1";
        bannerStatus = 2;
    } else if (bannerStatus === 2) {
        document.getElementById("imgban1").style.transition = "3s";
        document.getElementById("imgban1").style.opacity = "0";
        document.getElementById("imgban2").style.transition = "4s";
        document.getElementById("imgban2").style.opacity = "1";
        bannerStatus = 3;
    } else if (bannerStatus === 3) {
        document.getElementById("imgban2").style.transition = "3s";
        document.getElementById("imgban2").style.opacity = "0";
        document.getElementById("imgban3").style.transition = "4s";
        document.getElementById("imgban3").style.opacity = "1";
        bannerStatus = 1;
    }
}

function bannerLoop2() {
    if (bannerStatus === 1) {
        document.getElementById("imgban2").style.transition = "3s";
        document.getElementById("imgban2").style.opacity = "0";
        document.getElementById("imgban1").style.transition = "4s";
        document.getElementById("imgban1").style.opacity = "1";
        bannerStatus = 2;
    } else if (bannerStatus === 2) {
        document.getElementById("imgban3").style.transition = "3s";
        document.getElementById("imgban3").style.opacity = "0";
        document.getElementById("imgban2").style.transition = "4s";
        document.getElementById("imgban2").style.opacity = "1";
        bannerStatus = 3;
    } else if (bannerStatus === 3) {
        document.getElementById("imgban1").style.transition = "3s";
        document.getElementById("imgban1").style.opacity = "0";
        document.getElementById("imgban3").style.transition = "4s";
        document.getElementById("imgban3").style.opacity = "1";

        bannerStatus = 1;
    }
}

//Writing brands using javascript
const brands = ["Gucci","Fendi","Lacoste","Ralph Lauren","Gant"
    ,"Balenciaga","Off-White","Prada","Thommy Hilifiger","Disel","Hugo Boss","Armani","Versace","D&G"];
let countBrandsType = 0;
let indexBrandsType = 0;
let currentText = "";
let letters = "";
setInterval(function forward() {
    if (countBrandsType === brands.length) {
        countBrandsType = 0;
    }
    if (letters.length === currentText.length) {
        currentText = brands[Math.floor((Math.random() * brands.length))];
    }
    letters = currentText.slice(0,++indexBrandsType);
    document.getElementById("typeit").innerText = letters;
    if(letters.length === currentText.length) {
        countBrandsType++;
        indexBrandsType = 0;
    }
},400);


