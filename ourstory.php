<?php
require_once 'classes/ourstoryClass.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Our Story • Crocodile&trade;</title>
    <link rel="icon" href="images/favicon.png"/>
    <link rel="stylesheet" href="css/MainCss.css">
    <script src="https://kit.fontawesome.com/572ab64f26.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/story.css">
    <script src="JS/Throbber.js"></script>
    <script src="JS/toTopButton.js"></script>
    <link rel="stylesheet" href="CSS/toTopButton.css">
</head>
<body >
    <script src="JS/Head.js"> </script>
    <div class="mainDiv">
        <div id="title">
            <p class="mainTitle"> OUR STOR</p>
        </div>

        <?php
            $donarti = new ourstory(null,null,null,null,null);
            $query = 'ourstory';
            foreach ($donarti->createStories($query) as $haha) {?>
                <?php if($haha->getId() == 1 || $haha->getId() == 2 || $haha->getId() == 4 ||$haha->getId() == 5):
                 ?>
                <br />
                <br />
                <br />
                <br />
                <p class="headers"> <?php echo $haha->getTitle(); ?> </p>
                <br />
                <div class="img1">
                <img src="images/ourstoryPics/<?php echo $haha->getImage()?>">
                    <p class="text">
                        <?php echo $haha->getText() ?>
                    </p>
                </div>
            <?php  elseif($haha->getId() == 3 || $haha->getId() == 6):?>
                <br />
                <br />
                <br />
                <br />
                <p class="headers"> <?php echo $haha->getTitle() ?> </p>
                <br />
                <div id=<?php echo ($haha->getTitle() === ' ACCELERATED. ') ?  "mainPicture": "biggerPicture" ?>></div>
                <br />
                <div class="img1">
                    <p class="text">
                        <?php echo$haha->getText() ?>
                    </p>
                </div>
            <?php elseif($haha->getId() == 7): ?>
                <br />
                <br />
                <br />
                <br />
                <p class="headers"><?php echo $haha->getTitle() ?></p>
                <br />
                <div class="img1">
                    <img src="images/ourstoryPics/<?php echo $haha->getImage() ?>">
                    <p id="lastTitle"> GET TO KNOW OUR LEGAL BIT. </p>
                    <p class="text2">
                        <?php echo $haha->getText()?>
                    </p>
                </div>
        <?php  endif;}?>
</div>
    <script src="JS/Footer.js"></script>
</body>
</html>
