<?php

class LoginValidation {

    private $data;
    private $errors = [];
    private static $fields = ['Email', 'Password'];

    public function __construct($post_data){
        $this->data = $post_data;
    }

    public function validateForm(){

        foreach(self::$fields as $field){
            if(!array_key_exists($field, $this->data)){
                trigger_error("'$field' is not present in the data");
                return;
            }
        }

        $this->validateEmail();
        $this->validatePassword();
        return $this->errors;
    }

    private function validateEmail(){

        $val = trim($this->data['Email']);

        if(empty($val)){
            $this->addError('Email', 'Please enter your email');
        } else {
            if(!filter_var($val, FILTER_VALIDATE_EMAIL)){
                $this->addError('Email', 'Invalid email');
            }
        }
    }


    private function validatePassword(){

        $val = trim($this->data['Password']);

        if(empty($val)){
            $this->addError('Password', 'Please enter your password');
        } else {
            if(!preg_match('/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})/', $val)){
                $this->addError('Password','Min 8 character; min 1 uppercase; min 1 lowercase; min 1 number');
            }
        }

    }

    private function addError($key, $val){
        $this->errors[$key] = $val;
    }

}

?>