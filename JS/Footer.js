document.write("<footer>\n" +
    "        <div class=\"brand\">\n" +
    "            <img src=\"images/brands.png\" alt=\"brands\" class=\"fashion-brand\">\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"mainFooter\">\n" +
    "            <div class=\"footerInfo\">\n" +
    "                <h3 class=\"footerIteam\">\n" +
    "                    HELP & INFORMATION\n" +
    "                </h3>\n" +
    "                <div class=\"helpInformationInfo\">\n" +
    "                   <a href=\"#\" class=\"iteams\">Help</a>\n" +
    "                   <a href=\"#\" class=\"iteams\">Track Order</a>\n" +
    "                   <a href=\"#\" class=\"iteams\">Delivery & Returns</a>\n" +
    "                   <a href=\"#\" class=\"iteams\">10% Student Discount</a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        \n" +
    "            <div class=\"footerInfo\">\n" +
    "                <h3 class=\"footerIteam\">\n" +
    "                    ABOUT CROCODILE\n" +
    "                </h3>\n" +
    "                <div class=\"aboutCrocodileInfo\">\n" +
    "                   <a href=\"Aboutus.php\" class=\"iteams\">About Us</a>\n" +
    "                   <a href=\"#\" class=\"iteams\">Careers at CROCODILE</a>\n" +
    "                   <a href=\"#\" class=\"iteams\">Corporate Resposibility</a>\n" +
    "                   <a href=\"#\" class=\"iteams\">Investors Site</a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        \n" +
    "            <div class=\"footerInfo\">\n" +
    "                <h3 class=\"footerIteam\">\n" +
    "                    MORE FROM CROCODILE\n" +
    "                </h3>\n" +
    "                <div class=\"moreInfo\">\n" +
    "                   <a href=\"#\" class=\"iteams\">Mobile and CROCODILE Apps</a>\n" +
    "                   <a href=\"#\" class=\"iteams\">CROCODILE Marketplace</a>\n" +
    "                   <a href=\"#\" class=\"iteams\">Gift vouchers</a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        \n" +
    "            <div class=\"footerInfo\">\n" +
    "                <div id=\"myNav\" class=\"overlay\">\n" +
    "                    <a href=\"javascript:void(0)\" class=\"closebtn\" onclick=\"closeNav()\">&times;</a>\n" +
    "                    <div class=\"overlay-content\">\n" +
    "                        <a href=\"javascript:void(0)\" onclick=\"kosovaFlag(); closeNav()\">Kosova</a>\n" +
    "                        <a href=\"javascript:void(0)\" onclick=\"germanyFlag(); closeNav()\">Germany</a>\n" +
    "                        <a href=\"javascript:void(0)\" onclick=\"italyFlag(); closeNav()\">Italy</a>\n" +
    "                        <a href=\"javascript:void(0)\" onclick=\"franceFlag(); closeNav()\">France</a>\n" +
    "                        <a href=\"javascript:void(0)\" onclick=\"ukFlag(); closeNav()\">United Kingdom</a>\n" +
    "                        <a href=\"javascript:void(0)\" onclick=\"netherlandsFlag(); closeNav()\">Netherlands</a>\n" +
    "                        <a href=\"javascript:void(0)\" onclick=\"greeceFlag(); closeNav()\">Greece</a>\n" +
    "                        <a href=\"javascript:void(0)\" onclick=\"switzerlandFlag(); closeNav()\">Switzerland</a>\n" +
    "                        <a href=\"javascript:void(0)\" onclick=\"polandFlag(); closeNav()\">Poland</a>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <h3 class=\"footerIteam\">\n" +
    "                    SHOPPING FROM\n" +
    "                </h3>\n" +
    "                <a  onclick=\"openNav()\" class=\"changeCountry\">You're in <img src=\"images/Kosova.png\" alt=\"Country\" class=\"country\" id=\"countryFlag\"> | CHANGE</a>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"year\">\n" +
    "            <h5>&copy; 2020 CROCODILE</h5>\n" +
    "        </div>\n" +
    "    </footer>");

// JavaScript per pjesen e footer
function openNav() {
    document.getElementById("myNav").style.width = "100%";
}

function closeNav() {
    document.getElementById("myNav").style.width = "0%";
}

function kosovaFlag() {
    document.getElementById("countryFlag").src = "images/Kosova.png"
}

function germanyFlag() {
    document.getElementById("countryFlag").src = "images/Germany.png"
}

function italyFlag() {
    document.getElementById("countryFlag").src = "images/Italy.png"
}

function franceFlag() {
    document.getElementById("countryFlag").src = "images/France.png"
}

function ukFlag() {
    document.getElementById("countryFlag").src = "images/Uk.png"
}

function netherlandsFlag() {
    document.getElementById("countryFlag").src = "images/Netherlands.png"
}

function greeceFlag() {
    document.getElementById("countryFlag").src = "images/Greece.png"
}

function switzerlandFlag() {
    document.getElementById("countryFlag").src = "images/Switzerland.png"
}

function polandFlag() {
    document.getElementById("countryFlag").src = "images/Poland.png"
}

var acc = document.getElementsByClassName("footerIteam");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight) {
            panel.style.maxHeight = null;
        } else {
            panel.style.maxHeight = panel.scrollHeight + "px";
        }
    });
}
