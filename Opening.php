<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Welcome • Crocodile&trade;</title>
    <link rel="stylesheet" href="CSS/Opening.css">
    <link rel="stylesheet" href="CSS/MainCss.css">
    <script src="JS/OpeningJS.js"></script>
    <link rel="icon" href="images/favicon.png"/>
</head>
<body>
<div class="box">
    <div class="about">
        <div class="MainPage">
            <ul>
                <li class="opening">Welcome</li>
                <li class="second">Enter the Narnia</li>
                <li><button class="Buttons" onclick="go('shop')">Shop now</button></li>
            </ul>
        </div>
        <div class="Buttons-and-Text">
            <ul>
                <li>Who we are</li>
                <li class="second">The biggest fashion store</li>
                <li><button class="Buttons" onclick="go('ourstory')">Read the Crocodile&trade;</button></li>
            </ul>
            <ul>
                <li>Our Supporters</li>
                <li class="second">The best brands out there</li>
                <li><button class="Buttons" onclick="go('brands')">Our Brands</button></li>
            </ul>
        </div>
        <div class="figure"></div>
    </div>
</div>
</body>
</html>
