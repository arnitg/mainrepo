<?php

include './models/dbConnection.php';

class LoginController
{
    protected $db;

    public function __construct()
    {
        $this->db = new DbConnection();
    }

    public function login($request)
    {
        $query = $this->db->pdo->prepare('SELECT Firstname, Lastname, Email, Password, Admin FROM users WHERE Email = :Email');
        $query->bindParam(':Email', $request['Email']);
        $query->execute();

        $user = $query->fetch();

        if(count($user) > 0 && password_verify($request['Password'], $user['Password'])){
            $_SESSION['user_id'] = $user['ID'];
            $_SESSION['Firstname'] = $user['Firstname'];
            $_SESSION['Admin'] = $user['Admin'];

            if($user['Admin']){
                header("Location: http://localhost/mainrepo/adminArea.php");
            }else{
                header("Location: http://localhost/mainrepo/Main.php");
            }
        }else{
            echo "<script>alert('Email you entered does not exists! Please create a new user'); window.location.href=\"Register.php\";</script>";
        }
    }
}