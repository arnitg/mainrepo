<?php
require_once('models/dbConnection.php');
require_once('classes/product.php');
require_once('classes/photos.php');

if(isset($_GET['prod_id'])) {
    $jk = $_GET['prod_id'];
    $pictures = new photos(null,null,null);
    $product = new product(null,null,null,null,null);

    $tekstet =$product->getProducts($jk);
    $fotot = $pictures->getPictures($jk);

    $path =  $fotot[0]->getPath();
    $titulli =  $tekstet[0]->getDescription();
    $price = $tekstet[0]->getPrice();
    $brand = $tekstet[0]->getBrands();
    } else {
        echo 'ska id tnjejt' . $_GET['prod_id'];
    };
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo $titulli?></title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://kit.fontawesome.com/572ab64f26.js" crossorigin="anonymous"></script>
    <link rel="icon" href="images/favicon.png "/>
    <link rel="stylesheet" href="CSS/Opening.css">
    <link rel="stylesheet" href="CSS/MainCss.css">
    <link rel="stylesheet" href="CSS/ClothingInterfaceCss.css">
    <link rel="stylesheet" href="CSS/Buy.css">
    <style>
        <?php include 'CSS/Buy.css'?>
    </style>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <script src="JS/ClothingJS.js"></script>
    <script src="JS/Throbber.js"></script>
    <script src="JS/toTopButton.js"></script>
    <link rel="stylesheet" href="CSS/toTopButton.css">
</head>
<body>
    <script>
        <?php include "JS/Head.js" ?>
    </script>
    <main class="BuyMain">
        <div class="buyTitle">
            <h1><?php echo $titulli ?></h1>
        </div>
        <div class="content">
            <div class="carousel-container">
                <div class="carousel-slide" id="carousel">
                    <img src="images/<?php echo $fotot[count($fotot) - 1]->getPath()?>" alt="" id="lastClone">
                    <?php foreach ($fotot as $foto): ?>
                        <img src="images/<?php echo $foto->getPath()?>" alt="">
                    <?php endforeach;?>
                    <img src="images/<?php echo $fotot[0]->getPath()?>" alt="" id="firstClone">
                </div>
            </div>
            <button id="prevBtn"><i class="fa fa-arrow-left"></i></button>
            <button id="nextBtn"><i class="fa fa-arrow-right"></i></button>
            <div class="description-of-item">
                <h1 class="description-item"><?php echo $titulli ?></h1>
                <p class="price">Price: <?php echo $price ?></p>
                <p class="brand-type">Brand: <span><?php echo $brand ?></span></p>
                <p class="size">Size</p>
                <select name="drinks" required>
                    <option value="" disabled selected hidden>Choose your size</option>
                    <option value="">XS</option>
                    <option value="">S</option>
                    <option value="">M</option>
                    <option value="">L</option>
                    <option value="">X</option>
                    <option value="">XL</option>
                    <option value="">XXL</option>
                </select>
                <div class="bag-like">
                    <button class="add-to-bag">Add to Bag</button>
                    <button class="like"><i class="fa fa-heart"></i></button>
                </div>
            </div>
        </div>
    </main>
    <script src="JS/Buy.js"></script>
    <script src="JS/Footer.js"></script>
</body>
</html>
