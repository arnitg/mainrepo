<?php

require_once 'models/dbConnection.php';
class brands
{

    private $id;
    private $page;
    private $title;
    private $image;
    private $text;


    public function __construct($id, $page, $title, $image, $text)
    {
        $this->id = $id;
        $this->page = $page;
        $this->title = $title;
        $this->image = $image;
        $this->text = $text;
    }


    public function getId()
    {
        return $this->id;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function setPage($page)
    {
        $this->page = $page;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getText()
    {
        return $this->text;
    }

    public function setText($text)
    {
        $this->text = $text;
    }

    public function createBrands()
    {
        $emptyArray = (array)null;
        foreach ($this->connectBrands() as $item) {
            $test = new brands($item['id'], $item['page'], $item['title'], $item['image'], $item['text']);
            array_push($emptyArray, $test);
        }
        return $emptyArray;
    }

    public function connectBrands()
    {
        $dbcon = new DbConnection();
        $connection = $dbcon->getConnection();
        $query_students = 'select * from tblcontent where page=\'brands\' order by id';
        $student_statement = $connection->prepare($query_students);
        $student_statement->execute();
        $tekstet = $student_statement->fetchAll();
        $student_statement->closeCursor();
        return $tekstet;
    }
}
