<?php
require_once 'models/dbConnection.php';

class filePath
{
    private $fileName = 'C:\Users\ddona\Desktop\clothingImages\menswear\\';

    public function __construct()
    {
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    public function findFile($id,$path)
    {
        if(!empty($path) and !empty($id)) {
            $string = "update tblcontent set image='$path' WHERE id='$id'";
            $this->connectDb($string);
        }
    }


    public function updateOurStory($id,$title,$path,$text)
    {
        if(!empty($id)) {
            $query = $this->createQuery($id,$title,$path,$text);
            if (!empty($query)) {
                $this->connectDb($query);
            }
        }
    }

    public function createQuery($id,$title,$path,$text) {
        $string = null;
        if(!empty($title)) {
            $string = "update tblcontent set title='$title' WHERE id='$id'";
            if(!empty($path)) {
                $string = null;
                $string = "update tblcontent set title='$title' , image='$path' WHERE id='$id'";
                if(!empty($text)) {
                    $string = null;
                    $string = "update tblcontent set title='$title' , image='$path' , text='$text' WHERE id='$id'";
                }
            }
        }elseif(!empty($path)) {
            $string = null;
            $string = "update tblcontent set  image='$path' WHERE id='$id'";
            if(!empty($text)) {
                $string = null;
                $string = "update tblcontent set image='$path' , text='$text' WHERE id='$id'";
            }
        }elseif(!empty($text)) {
            $string = null;
            $string = "update tblcontent set text='$text' WHERE id='$id'";
        }
        return $string;
    }

    public function connectDb($string)
    {
        $dbcon = new DbConnection();
        $connection = $dbcon->getConnection();
        try {
            $query_students = $string;
            $student_statement = $connection->prepare($query_students);
            $student_statement->execute();
            $student_statement->closeCursor();
        }catch (PDOException $pdo)
        {
            die("Error could not be updated");
        }
    }
}

?>