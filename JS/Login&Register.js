function validateFirstname() {
    var firstname = document.getElementById('Firstname').value;
    var checkFirstname = new RegExp(/^[a-zA-Z]+$/);

    if(checkFirstname.test(firstname)){
        document.getElementById('firstnameProblem').innerHTML = "";
        document.getElementById('Firstname').style.border = "2px solid green";
    }else if(firstname === ""){
        document.getElementById('firstnameProblem').innerHTML = "Please enter your firstname";
        document.getElementById('Firstname').style.border = "2px solid red";
        return false;
    }else{
        var text = document.getElementById('firstnameProblem');
        text.innerHTML = "Firstname must contain only letters";
        text.style.color = "orange";
        document.getElementById('Firstname').style.border = "2px solid orange";
        return false;
    }
}

function validateLastname() {
    var lastname = document.getElementById('Lastname').value;
    var checkLastname = new RegExp(/^[a-zA-Z]+$/);

    if(checkLastname.test(lastname)){
        document.getElementById('lastnameProblem').innerHTML = "";
        document.getElementById('Lastname').style.border = "2px solid green";
    }else if(lastname === ""){
        document.getElementById('lastnameProblem').innerHTML = "Please enter your lastname";
        document.getElementById('Lastname').style.border = "2px solid red";
        return false;
    }else{
        var text = document.getElementById('lastnameProblem');
        text.innerHTML = "Lastname must contain only letters";
        text.style.color = "orange";
        document.getElementById('Lastname').style.border = "2px solid orange";
        return false;
    }
}

function validateEmail(){
    var email = document.getElementById('Email').value;
    var checkEmail = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);

    if(checkEmail.test(email)){
        document.getElementById('emailProblem').innerHTML = "";
        document.getElementById('Email').style.border = "2px solid green";
    }else if(email === ""){
        document.getElementById('emailProblem').innerHTML = "Please enter your email";
        document.getElementById('Email').style.border = "2px solid red";
        return false;
    }else{
        var text = document.getElementById('emailProblem');
        text.innerHTML = "Invalid email";
        text.style.color = "orange";
        document.getElementById('Email').style.border = "2px solid orange";
        return false;
    }
}

function validatePassword(){
    var password = document.getElementById('Password').value;
    var checkPassword = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");

    if(checkPassword.test(password)){
        document.getElementById('passwordProblem').innerHTML = "";
        document.getElementById('Password').style.border = "2px solid green";
    }else if(password === ""){
        document.getElementById('passwordProblem').innerHTML = "Please enter your password";
        document.getElementById('Password').style.border = "2px solid red";
        return false;
    }else{
        var text = document.getElementById('passwordProblem');
        text.innerHTML = "Min 8 character; min 1 uppercase; min 1 lowercase; min 1 number";
        text.style.color = "orange";
        document.getElementById('Password').style.border = "2px solid orange";
        return false;
    }
}

function validatePasswordConfirmation(){
    var password = document.getElementById('Password').value;
    var passwordConfirmation = document.getElementById('PasswordConfirmation').value;
    var checkPassword = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");

    if(checkPassword.test(passwordConfirmation) && password === passwordConfirmation){
        document.getElementById('passwordConfirmationProblem').innerHTML = "";
        document.getElementById('PasswordConfirmation').style.border = "2px solid green";
    }else if(passwordConfirmation === ""){
        document.getElementById('passwordConfirmationProblem').innerHTML = "Please enter your password";
        document.getElementById('PasswordConfirmation').style.border = "2px solid red";
        return false;
    }else{
        var text = document.getElementById('passwordConfirmationProblem');
        text.innerHTML = "Password does not match";
        text.style.color = "orange";
        document.getElementById('PasswordConfirmation').style.border = "2px solid orange";
        return false;
    }
}

function validateDay(){
    var day = document.getElementById('day').value;

    if(day !== "day"){
        document.getElementById('dayMissing').innerHTML = "";
        document.getElementById('day').style.border = "2px solid green";
    }else{
        document.getElementById('dayMissing').innerHTML = "Day is missing";
        document.getElementById('day').style.border = "2px solid red";
        return false;
    }
}

function validateMonth(){
    var month= document.getElementById('month').value;

    if(month !== "month"){
        document.getElementById('monthMissing').innerHTML = "";
        document.getElementById('month').style.border = "2px solid green";
    }else{
        document.getElementById('monthMissing').innerHTML = "Month is missing";
        document.getElementById('month').style.border = "2px solid red";
        return false;
    }
}

function validateYear(){
    var year = document.getElementById('year').value;

    if(year !== "year"){
        document.getElementById('yearMissing').innerHTML = "";
        document.getElementById('year').style.border = "2px solid green";
    }else{
        document.getElementById('yearMissing').innerHTML = "Year is missing";
        document.getElementById('year').style.border = "2px solid red";
        return false;
    }
}

function validateOption(){
    var Womenswear = document.getElementById('Womenswear').checked;
    var Menswear = document.getElementById('Menswear').checked;

    if(Womenswear !== false || Menswear !== false){
        document.getElementById('optionProblem').innerHTML = "";
        document.getElementById('Womenswear').style.border = "2px solid red";
        document.getElementById('Menswear').style.border = "2px solid red";
    }
    else{
        document.getElementById('optionProblem').innerHTML = "please choose one of the options";
        document.getElementById('Womenswear').style.border = "2px solid green";
        document.getElementById('Menswear').style.border = "2px solid green";
        return false;
    }
}

function validateEmailReset(){
    var emailReset = document.getElementById('EmailReset').value;
    var checkEmailReset = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);

    if(checkEmailReset.test(emailReset)){
        document.getElementById('emailResetProblem').innerHTML = "";
        document.getElementById('EmailReset').style.border = "2px solid green";
    }else if(email === ""){
        document.getElementById('emailResetProblem').innerHTML = "Please enter your email";
        document.getElementById('EmailReset').style.border = "2px solid red";
        return false;
    }else{
        var text = document.getElementById('emailResetProblem');
        text.innerHTML = "Invalid email";
        text.style.color = "orange";
        document.getElementById('EmailReset').style.border = "2px solid orange";
        return false;
    }
}

function cancelReset(){
    document.getElementById('light').style.display='none';
    document.getElementById('fade').style.display='none'
}

function openReset(){
    document.getElementById('light').style.display='block';
    document.getElementById('fade').style.display='block'
}

