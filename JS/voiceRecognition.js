var SpeechRecognition = SpeechRecognition =  webkitSpeechRecognition;
var SpeechGrammarList = SpeechGrammarList = webkitSpeechGrammarList;

var grammar = '#JSGF V1.0;';

var recognition = new SpeechRecognition();
var speechRecognitionGrammarList = new SpeechGrammarList();
speechRecognitionGrammarList.addFromString(grammar,1);
recognition.grammars = speechRecognitionGrammarList;
recognition.lang = 'en-US';
recognition.interimResults = false;

recognition.onresult = function () {
    var last = event.results.length - 1;
    var command = event.results[last][0].transcript;
    console.log(command.toLowerCase());
    if(command.toLowerCase() === "go to men's clothing" || command.toLowerCase() === "men"){
        window.location = "ClothingInterface.php?category=Men";
    } else if(command.toLowerCase() === "go to women's clothing" || command.toLowerCase() === "women") {
        window.location = "ClothingInterface.php?category=Women";
    }
};
recognition.onspeechend = function () {
    recognition.stop();
};
recognition.onerror = function () {
    document.alert("alert")
};
function goBro() {
    recognition.start();
}