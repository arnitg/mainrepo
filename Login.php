<?php
require './controllers/LoginController.php';
require 'controllers/LoginValidation.php';

$errors = [];

if(isset($_POST['submit'])){

    $user = new LoginController;
    $loginValidation = new LoginValidation($_POST);
    $errors = $loginValidation->validateForm();

    $email = htmlspecialchars($_POST['Email']);

    if(sizeof($errors) == 0){
        $user->login($_POST);
    }
}

?>

<!DOCTYPE html>
<html>
<head>
    <title>Sign In • Crocodile&trade;</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="icon" href="images/favicon.png"/>
    <script src="https://kit.fontawesome.com/572ab64f26.js" crossorigin="anonymous"></script>
    <script src="JS/Login&Register.js"></script>
    <style>
        <?php include 'CSS/Login&Register.css'?>
    </style>
</head>
<body>
<header>
    <h1>CROCODILE&trade;</h1>
</header>
<main>
    <div class="register">
        <div class="registerOrLogin" id="newToCrocodile">
            <a id="registerTitle" href="Register.php" onload="loginOrRegister()">
                NEW TO CROCODILE?
            </a>
        </div>

        <div class="registerOrLogin" id="alreadyRegistered" onload="loginOrRegister()">
            <a id="loginTitle" href="Login.php">
                ALREADY REGISTERED?
            </a>
        </div>

        <h3>
            SIGN IN WITH ...
        </h3>

        <button class="signInWith" onclick="MyWindow=window.open('https://www.facebook.com/login/reauth.php?app_id=1427730400838960&signed_next=1&next=https%3A%2F%2F','MyWindow','width=600,height=650');">
            <img src="images/facebook.png" alt="">
            <h3>Facebook</h3>
        </button>

        <button class="signInWith" onclick="MyWindow=window.open('https://accounts.google.com/signin','MyWindow','width=600,height=650');">
            <img src="images/google.png" alt="">
            <h3>Google</h3>
        </button>

        <button class="signInWith" onclick="MyWindow=window.open('https://twitter.com/login' ,'MyWindow','width=600,height=650');">
            <img src="images/Twitter.png" alt="">
            <h3>Twitter</h3>
        </button>
        <div class="hrdivider">
            <hr/>
            <span>OR</span>
        </div>

        <h3 class="EAsignUp">
            SIGN IN WITH EMAIL
        </h3>

        <div class="form">
            <form action="" name="registerForm" method="POST" onsubmit="return validateEmail() || validatePassword()">
                <label for="Email">Email:</label><br>
                <input class="personalInput" type="email" name="Email" id="Email"   onchange="return validateEmail()" value="<?php echo $email ?? '' ?>"><br>
                <p class="registerProblem" id="emailProblem"><?php echo $errors['Email'] ?? '' ?></p>
                <label for="Password">Password:</label><br>
                <input class="personalInput" type="password" name="Password" id="Password"   onchange="return validatePassword()"><br>
                <p class="registerProblem" id="passwordProblem"><?php echo $errors['Password'] ?? '' ?></p>
                <a class="forgotPass" href="javascript:void(0)" onclick="openReset()">Forgot Password?</a>
                <br>
                <input type="submit" name="submit" value="SIGN IN" class="submitbutton">

                <div id="light" class="white_content">
                    <img src="images/x.png" alt="cancel" class="cancelImg" onclick="cancelReset()">

                    <h1>
                        FORGOT PASSWORD
                    </h1>
                    <p>
                        Please enter your email address below and we will send you information to change your password.
                    </p>
                    <form action="" name="resetForm" method="POST" onsubmit="return validateEmail()">
                        <label for="EmailReset">Email:</label><br>
                        <input class="personalInput" type="email" name="EmailReset" id="EmailReset" onchange="return validateEmailReset()"><br>
                        <p class="registerProblem" id="emailResetProblem"></p>
                        <input type="reset" value="RESET" class="resetbutton" onclick="cancelReset()">
                    </form>
                </div>
                <div id="fade" class="black_overlay"></div>

                <!-- <div id="resetPassword" class="forgotPassword" hidden>
                    <h1>
                        FORGOT PASSWORD
                    </h1>
                    <p>
                        Please enter your email address below and we will send you information to change your password.
                    </p>
                    <form action="" name="registerForm" method="POST" onsubmit="return validateEmail() || validatePassword()">
                        <label for="Email">Email:</label><br>
                        <input class="personalInput" type="email" name="Email" id="Email"   onchange="return validateEmail()"><br>
                        <p class="registerProblem" id="emailProblem"></p>
                        <input type="submit" value="RESET" class="resetbutton">
                    </form>
                </div> -->
            </form>
        </div>
    </div>
</main>
</body>
</html>