<?php
require_once 'classes/ourstoryClass.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Brands • Crocodile&trade;</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="icon" href="images/favicon.png"/>
    <link rel="stylesheet" href="CSS/MainCss.css">
    <script src="https://kit.fontawesome.com/572ab64f26.js" crossorigin="anonymous"></script>
    <script src="JS/Throbber.js"></script>
    <link rel="stylesheet" href="CSS/Brands.css">
    <script type="text/javascript" src="JS/Brands.js"></script>
    <script src="JS/toTopButton.js"></script>
    <link rel="stylesheet" href="CSS/toTopButton.css">
</head>
<body>
    <script>
        <?php include "JS/Head.js" ?>
    </script>
    <div class="mainPhotoBrands">
        <?php
        $query='brands';
        $donarti = new ourstory(null,null,null,null,null);
        foreach ($donarti->createStories($query)  as $haha):?>
        <?php if($haha->getTitle() === 'Crocodile Brands'): ?>
        <div class="crocodileBrands">
            <p id="brandsAnimation">
                <?php echo $haha->getTitle() ?>
            </p>
            <h4><?php echo $haha->getText()?></h4>
            <h4 class="typing" id="typeit"></h4>
        </div>
    </div>
    <img class="absolute-img" src="images/<?php echo $haha->getImage()?>" alt="Brands...">
    <?php elseif ($haha->getTitle() === 'A site of the future'): ?>
    <div class="whiteDiv">
        <div class="Commercial">
            <div class="Commercial-left">
                <div class="up-text">
                    <p class="titleBrands"><?php echo $haha->getTitle()?></p>
                    <p class="textBrands">
                       <?php echo $haha->getText()?>
                    </p>
                </div>
                <div class="down-photo">
                    <div class="zPhoto <?php echo $haha->getImage()?>"></div>
                </div>
            </div>
            <div class="Commercial-right">
                <div class="photo-commercial">

                </div>
            </div>
        </div>
    </div>
    <?php elseif ($haha->getTitle() === 'Our Protection Policies'): ?>
    <div class="back-policies">
        <div class="protection-policies" style="background-image: url(<?php echo 'images/' . $haha->getImage()?>)"></div>
        <div class="textDiv">
        <p class="titleBrands"><?php echo $haha->getTitle()?></p>
        <p class="textBrands">
            <?php echo $haha->getText()?>
        </p>
        </div>
    </div>
    <?php elseif ($haha->getTitle()=== 'RENÉ LACOSTE'):?>
    <div class="quote-space">
        <div class="quote">
            <p>
                <q></q>
                <?php echo $haha->getText()?>
                <q></q> <br />
                <?php echo $haha->getTitle()?>
            </p>
        </div>
    </div>
    <?php endif; endforeach; ?>
    <div class="main-banner" id="main-banner" onmouseenter="stop();" onmouseleave="play()">
        <div class="imgbanbtn imagebanbtn-prev" id="imagebanbtn-prev" onclick="prev()"></div>
        <div class="imgbanbtn imagebanbtn-next" id="imagebanbtn-next" onclick="next()"></div>
        <div class="imgban" id="imgban3">
            <div class="imgban-box">
            </div>
        </div>
        <div class="imgban" id="imgban2">
            <div class="imgban-box">
            </div>
        </div>
        <div class="imgban" id="imgban1">
            <div class="imgban-box">
            </div>
        </div>
    </div>
    <div class="rights">
        <p>All rights reserved to crocodile fashion!</p>
    </div>
    <script src="JS/Footer.js"></script>
</body>
</html>
