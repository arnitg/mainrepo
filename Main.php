<!--                    <a href="/BuyInterface.html/3">Link</a>-->
<?php
require_once 'classes/ourstoryClass.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Shop • Crocodile&trade;</title>
    <link rel="stylesheet" href="CSS/swiper.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://kit.fontawesome.com/572ab64f26.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="CSS/Opening.css">
    <link rel="icon" href="images/favicon.png"/>
    <script src="JS/js.js"></script>
    <script src="JS/voiceRecognition.js"></script>
    <script src="JS/countdown.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <script src="JS/Throbber.js"></script>
    <script src="JS/toTopButton.js"></script>
    <link rel="stylesheet" href="CSS/toTopButton.css">
    <style>
        <?php include "CSS/App.css"?>
        <?php include "CSS/MainCss.css"?>
    </style>
</head>
<body>
    <script>
        <?php include "JS/Head.js" ?>
    </script>
    <div class="sale">
        <img src="images/zbritje-01.png">
        <p class="countdown" id="countdown"></p>
    </div>
    <div class="main-banner" id="main-banner" onmouseenter="stop();" onmouseleave="play()">
        <div class="imgbanbtn imagebanbtn-prev" id="imagebanbtn-prev" onclick="prev()"></div>
        <div class="imgbanbtn imagebanbtn-next" id="imagebanbtn-next" onclick="next()"></div>
        <?php
        $donarti = new ourstory(null,null,null,null,null);
        foreach ($donarti->createStories('main') as $haha):?>
        <?php if ($haha->getTitle() === 'Marc Jacobs' || $haha->getTitle() === 'Kenzo Takada' ||$haha->getTitle() === 'John Galliano'):?>
        <?php
            $typeofid = 0;
            if($haha->getTitle() == 'Marc Jacobs') {
                $typeofid = 3;
            } else if($haha->getTitle() === 'Kenzo Takada') {
                $typeofid = 2;
            } else {
                $typeofid = 1;
            }
        ?>
        <div class="imgban" id="imgban<?php echo $typeofid?>" style="background-image: url(<?php echo 'images/' .  $haha->getImage() ?>);">
            <div class="imgban-box">
                <h2><?php echo $haha->getText()?></h2>
                <p><?php echo $haha->getTitle()?></p>
            </div>
        </div>
            <?php endif; endforeach;?>
    </div>

    <div>
        <?php foreach ($donarti->createStories('video') as $haha):?>
        <video  width="100%" height="551vh"  autoplay muted loop>
            <source src="Video/<?php echo $haha->getImage()?>" type="video/mp4">
        </video>
        <?php endforeach;?>
    </div>

    <div class="animated-photos">
        <div class="text-Crocodile">
            <h1>Crocodile</h1>
            <h2>THE PERFECT UNION BETWEEN HARMONY AND ELEGANCE</h2>
        </div>
        <div class="photos">
            <div class="men-div">
                <div class="ThePhoto-1" style="background-image: url(images/ourstoryPics/redlight.png);" onmouseenter="bigg(this,0)" onmouseleave="smaller(this,0)">
                    <ul class="skrr">
                        <li class="opening">Men Clothing</li>
                        <li><button class="Buttons" onclick="goM()">Shop now</button></li>
                    </ul>
                    <ul class="mobile">
                        <li class="opening">Men Clothing</li>
                        <li><button class="Buttons" onclick="goM()">Shop now</button></li>
                    </ul>
                </div>
            </div>
            <div class="women-div" >
                <div class="ThePhoto-2" style="background-image: url(images/ourstoryPics/truckgirl.jpg);" onmouseenter="bigg(this,1)" onmouseleave="smaller(this,1)">
                    <ul class="skrr">
                        <li class="opening">Women Clothing</li>
                        <li><button class="Buttons" onclick="goM()">Shop now</button></li>
                    </ul>
                    <ul class="mobile">
                        <li class="opening">Women Clothing</li>
                        <li><button class="Buttons" onclick="goM()">Shop now</button></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="Highlight"><p>Highlights</p></div>
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <?php
                foreach ($donarti->createStories('slide') as $haha):?>
                <div class="swiper-slide">
                    <div class="imgBx">
                        <img src="<?php echo  'images/'.$haha->getImage();?>">
                    </div>
                    <div class="Details">
                        <h3><?php echo $haha->getTitle();?></h3>
                    </div>
                </div>
                <?php  endforeach;?>
    </div>
        </div>
        <div class="swiper-pagination"></div>
    </div>
    <div class="swiper-helper"></div>
    <script src="JS/swiper.min.js"></script>
    <script src="JS/Swiper.js"></script>
    <script>
        <?php include 'JS/Footer.js';?>
    </script>
</body>
</html>
