
var countDownDate = new Date("Jul 17, 2020 14:00:00" ).getTime();
var myfunc = setInterval(function() {
    var now = new Date().getTime();
    var timeleft = countDownDate - now;
    var days = Math.floor(timeleft / (1000 * 60 * 60 * 24));
    var hours = Math.floor((timeleft % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((timeleft % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((timeleft % (1000 * 60)) / 1000);
    document.getElementById("countdown").innerHTML = days + "d " + hours + "h " + minutes + "m " + seconds + "s ";
    if (timeleft < 0) {
        clearInterval(myfunc);
        document.getElementById("countdown").innerHTML = "";
        document.getElementById("countdown").innerHTML = "SALE IS UP!!";
    }
}, 1000);