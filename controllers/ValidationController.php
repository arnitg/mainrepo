<?php

class Validation {

    private $data;
    private $errors = [];
    private static $fields = ['Firstname', 'Lastname', 'Email', 'Password', 'PasswordConfirmation', 'BirthDay'];

    public function __construct($post_data){
        $this->data = $post_data;
    }

    public function validateForm(){

        foreach(self::$fields as $field){
            if(!array_key_exists($field, $this->data)){
                trigger_error("'$field' is not present in the data");
                return;
            }
        }

        $this->validateFirstname();
        $this->validateLastname();
        $this->validateEmail();
        $this->validatePassword();
        $this->validatePasswordConfirmation();
        $this->validateBirthDay();
        return $this->errors;
    }

    private function validateFirstname(){

        $val = trim($this->data['Firstname']);

        if(empty($val)){
            $this->addError('Firstname', 'Please enter your firstname');
        } else {
            if(!preg_match('/^[a-zA-Z]+$/', $val)){
                $this->addError('Firstname','Firstname must contain only letters');
            }
        }
    }

    private function validateLastname(){

        $val = trim($this->data['Lastname']);

        if(empty($val)){
            $this->addError('Lastname', 'Please enter your lastname');
        } else {
            if(!preg_match('/^[a-zA-Z]+$/', $val)){
                $this->addError('Lastname','Lastname must contain only letters');
            }
        }

    }

    private function validateEmail(){

        $val = trim($this->data['Email']);

        if(empty($val)){
            $this->addError('Email', 'Please enter your email');
        } else {
            if(!filter_var($val, FILTER_VALIDATE_EMAIL)){
                $this->addError('Email', 'Invalid email');
            }
        }
    }


    private function validatePassword(){

        $val = trim($this->data['Password']);

        if(empty($val)){
            $this->addError('Password', 'Please enter your password');
        } else {
            if(!preg_match('/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})/', $val)){
                $this->addError('Password','Min 8 character; min 1 uppercase; min 1 lowercase; min 1 number');
            }
        }

    }

    private function validatePasswordConfirmation(){

        $password = trim($this->data['Password']);
        $passwordConfirmation = trim($this->data['PasswordConfirmation']);

        if(empty($passwordConfirmation)){
            $this->addError('PasswordConfirmation', 'Please enter your password');
        } else if($password != $passwordConfirmation){
                $this->addError('PasswordConfirmation','Password does not match');
        }

    }

    private function validateBirthDay(){

        $day = $this->data['BirthDay'];
        $month = $this->data['BirthMonth'];
        $year = $this->data['BirthYear'];

        if($day == 'day' || $month == 'month' || $year == "year"){
            $this->addError('BirthDay', 'Please enter a full birthday');
        }

    }

    private function addError($key, $val){
        $this->errors[$key] = $val;
    }

}

?>